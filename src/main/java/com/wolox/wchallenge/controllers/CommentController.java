package com.wolox.wchallenge.controllers;

import com.wolox.wchallenge.models.dto.CommentDto;
import com.wolox.wchallenge.services.external.CommentExternalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comments")
@Api("Comments of jsonplaceholder")
public class CommentController {

    private final CommentExternalService commentExternalService;

    @Autowired
    public CommentController(CommentExternalService commentExternalService) {
        this.commentExternalService = commentExternalService;
    }

    @GetMapping
    @ApiOperation(value = "Get comments", response = CommentDto[].class)
    public List<CommentDto> getAllComments() {
        return commentExternalService.getAllComment();
    }

    @GetMapping("all/name")
    @ApiOperation(value = "Get comments by name", response = CommentDto.class)
    public List<CommentDto> getCommentByName(@RequestParam(value = "name") String name) {
        return commentExternalService.getCommentByName(name);
    }

    @GetMapping("all/user/{id}")
    @ApiOperation(value = "Get comments by user id", response = CommentDto[].class)
    public List<CommentDto> getCommentsByUserId(@PathVariable(value = "id") Long id) {
        return commentExternalService.getCommentByIdUser(id);
    }
}
