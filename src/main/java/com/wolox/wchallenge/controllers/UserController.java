package com.wolox.wchallenge.controllers;

import com.wolox.wchallenge.models.dto.UserDto;
import com.wolox.wchallenge.services.external.UserExternalService;
import com.wolox.wchallenge.services.internal.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@Api("Users of jsonplaceholder")
public class UserController {

    private final UserExternalService userExternalService;

    private final UserService userService;

    @Autowired
    public UserController(UserExternalService userExternalService, UserService userService) {
        this.userExternalService = userExternalService;
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(value = "Get users", response = UserDto[].class)
    public List<UserDto> getAllUser() {
        return userExternalService.getAllUsers();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get user by id", response = UserDto.class)
    public UserDto getUserById(@PathVariable(value = "id") Long id) {
        return userExternalService.getUserById(id);
    }

    @GetMapping("all/album/{id}/permission/{permission}")
    @ApiOperation(value = "Get users by album with permission", response = UserDto[].class)
    public List<UserDto> getUserByAlbumWithPermission(@PathVariable(value = "id") Long id,
                                                      @PathVariable(value = "permission") Integer permission) {

        return userService.getUsersByAlbumAndPermission(id, permission);
    }


}
