package com.wolox.wchallenge.controllers;

import com.wolox.wchallenge.exceptions.PermissionException;
import com.wolox.wchallenge.models.dto.PermissionAlbumUserDto;
import com.wolox.wchallenge.models.dto.PermissionDto;
import com.wolox.wchallenge.services.internal.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/permission")
@Api("Permission Albums")
public class PermissionAlbumUserController {

    private final PermissionService permissionService;

    @Autowired
    public PermissionAlbumUserController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @PostMapping("shared/album")
    @ApiOperation(value = "Associate an album to a user with permission", response = PermissionAlbumUserDto.class)
    public PermissionAlbumUserDto shareAlbumWithUser(@RequestBody PermissionDto permission) {
        return permissionService.sharedAlbum(
                permission.getIdAlbum(),
                permission.getIdUser(),
                permission.getPermission())
                .orElseThrow(() -> new PermissionException("Denied permission"));
    }

    @PutMapping("shared/album")
    @ApiOperation(value = "Update permission for a user ", response = PermissionAlbumUserDto.class)
    public PermissionAlbumUserDto updatePermission(@RequestBody PermissionDto permission) {
        return permissionService.updatePermission(
                permission.getIdAlbum(),
                permission.getIdUser(),
                permission.getPermission())
                .orElseThrow(() -> new PermissionException("Error changing permissions"));
    }

}
