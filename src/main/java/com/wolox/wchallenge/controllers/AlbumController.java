package com.wolox.wchallenge.controllers;

import com.wolox.wchallenge.models.dto.AlbumDto;
import com.wolox.wchallenge.services.external.AlbumExternalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/albums")
@Api("Albums of jsonplaceholder")
public class AlbumController {

    private final AlbumExternalService albumExternalService;

    @Autowired
    public AlbumController(AlbumExternalService albumExternalService) {
        this.albumExternalService = albumExternalService;
    }

    @GetMapping
    @ApiOperation(value = "Get albums", response = AlbumDto[].class)
    public List<AlbumDto> getAllAlbum() {
        return albumExternalService.getAllAlbum();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get album by id", response = AlbumDto.class)
    public AlbumDto getAlbumById(@PathVariable(value = "id") Long id) {
        return albumExternalService.getAlbumById(id);
    }

    @GetMapping("all/user/{id}")
    @ApiOperation(value = "Get albums by user id", response = AlbumDto[].class)
    public List<AlbumDto> getAlbumByIdUser(@PathVariable(value = "id") Long id) {
        return albumExternalService.getAlbumByIdUser(id);
    }

}
