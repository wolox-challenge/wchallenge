package com.wolox.wchallenge.controllers;

import com.wolox.wchallenge.models.dto.PhotoDto;
import com.wolox.wchallenge.services.external.PhotoExternalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/photos")
@Api("Photos of jsonplaceholder")
public class PhotoController {

    private final PhotoExternalService photoExternalService;

    @Autowired
    public PhotoController(PhotoExternalService photoExternalService) {
        this.photoExternalService = photoExternalService;
    }

    @GetMapping
    @ApiOperation(value = "Get photos", response = PhotoDto[].class)
    public List<PhotoDto> getAllPhotos() {
        return photoExternalService.getAllPhotos();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get photo by id", response = PhotoDto.class)
    public PhotoDto getPhotosById(@PathVariable(value = "id") Long id) {
        return photoExternalService.getPhotoById(id);
    }

    @GetMapping("all/album/{id}")
    @ApiOperation(value = "Get photos by album id", response = PhotoDto[].class)
    public List<PhotoDto> getPhotosByAlbumId(@PathVariable(value = "id") Long id) {
        return photoExternalService.getPhotosByAlbum(id);
    }

    @GetMapping("all/user/{id}")
    @ApiOperation(value = "Get photos by user id", response = PhotoDto[].class)
    public List<PhotoDto> getPhotosByUserId(@PathVariable(value = "id") Long id) {
        return photoExternalService.getPhotosByUser(id);
    }
}
