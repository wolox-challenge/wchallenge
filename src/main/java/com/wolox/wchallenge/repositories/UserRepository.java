package com.wolox.wchallenge.repositories;

import com.wolox.wchallenge.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByIdEndpoint(Long id);

    @Query(nativeQuery = false,
            value = "SELECT pau.user " +
                    "FROM PermissionAlbumUser pau " +
                    "WHERE  pau.album.idEndpoint = :idAlbum " +
                    "and pau.permissions = :permission")
    List<User> getUsersByAlbumWithPermission(@Param(value = "idAlbum") Long idAlbum,
                                             @Param(value = "permission") String permission);

}
