package com.wolox.wchallenge.repositories;

import com.wolox.wchallenge.models.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

    Optional<Album> findByIdEndpoint(Long id);
}
