package com.wolox.wchallenge.repositories;

import com.wolox.wchallenge.models.PermissionAlbumUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PermissionAlbumUserRepository extends JpaRepository<PermissionAlbumUser, Long> {

    Optional<PermissionAlbumUser> findByAlbum_IdEndpointAndUser_IdEndpoint(Long idAlbum, Long idUser);

    Boolean existsByAlbum_IdEndpointAndUser_IdEndpoint(Long idAlbum, Long idUser);
}
