package com.wolox.wchallenge.exceptions;

public class PhotoException extends RuntimeException {

    public PhotoException(String message) {
        super(message);
    }
}
