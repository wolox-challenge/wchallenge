package com.wolox.wchallenge.exceptions;

public class AlbumException extends RuntimeException {

    public AlbumException(String message) {
        super(message);
    }
}
