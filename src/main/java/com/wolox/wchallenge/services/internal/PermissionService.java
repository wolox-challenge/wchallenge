package com.wolox.wchallenge.services.internal;

import com.wolox.wchallenge.models.dto.PermissionAlbumUserDto;

import java.util.Optional;

public interface PermissionService {

    Optional<PermissionAlbumUserDto> sharedAlbum(Long idAlbum, Long idUser, Integer permission);

    Optional<PermissionAlbumUserDto> updatePermission(Long idAlbum, Long idUser, Integer permission);
}
