package com.wolox.wchallenge.services.internal;

import com.wolox.wchallenge.models.Album;


public interface AlbumService {

    Album albumVerified(Album album);
}
