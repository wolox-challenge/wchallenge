package com.wolox.wchallenge.services.internal.impl;

import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.repositories.AlbumRepository;
import com.wolox.wchallenge.services.internal.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlbumServiceImpl implements AlbumService {

    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumServiceImpl(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    @Override
    public Album albumVerified(Album album) {
        return albumRepository.findByIdEndpoint(album.getIdEndpoint())
                .orElseGet(() -> save(album));

    }

    private Album save(Album album) {
        return albumRepository.save(album);
    }
}
