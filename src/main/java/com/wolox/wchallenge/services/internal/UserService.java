package com.wolox.wchallenge.services.internal;

import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;

import java.util.List;

public interface UserService {

    User userVerified(User user);

    List<UserDto> getUsersByAlbumAndPermission(Long idAlbum, Integer permission);
}
