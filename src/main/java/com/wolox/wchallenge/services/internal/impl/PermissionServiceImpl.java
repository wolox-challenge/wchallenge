package com.wolox.wchallenge.services.internal.impl;

import com.wolox.wchallenge.exceptions.PermissionException;
import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.models.PermissionAlbumUser;
import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.PermissionAlbumUserDto;
import com.wolox.wchallenge.models.enums.PermissionEnum;
import com.wolox.wchallenge.models.transform.PermissionAlbumUserTransform;
import com.wolox.wchallenge.repositories.PermissionAlbumUserRepository;
import com.wolox.wchallenge.services.external.AlbumExternalService;
import com.wolox.wchallenge.services.external.UserExternalService;
import com.wolox.wchallenge.services.internal.AlbumService;
import com.wolox.wchallenge.services.internal.PermissionService;
import com.wolox.wchallenge.services.internal.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PermissionServiceImpl implements PermissionService {

    private final PermissionAlbumUserRepository permissionAlbumUserRepository;

    private final AlbumService albumService;

    private final AlbumExternalService albumExternalService;

    private final UserExternalService userExternalService;

    private final UserService userService;

    private final PermissionAlbumUserTransform transform;

    @Autowired
    public PermissionServiceImpl(PermissionAlbumUserRepository permissionAlbumUserRepository, AlbumService albumService, UserService userService, PermissionAlbumUserTransform permissionAlbumUserTransform, AlbumExternalService albumExternalService, UserExternalService userExternalService) {
        this.permissionAlbumUserRepository = permissionAlbumUserRepository;
        this.albumService = albumService;
        this.userService = userService;
        this.transform = permissionAlbumUserTransform;
        this.albumExternalService = albumExternalService;
        this.userExternalService = userExternalService;
    }

    @Override
    public Optional<PermissionAlbumUserDto> sharedAlbum(Long idAlbum,
                                                        Long idUser,
                                                        Integer permission) {

        Album album = albumExternalService.getAlbum(idAlbum);

        User user = userExternalService.getUser(idUser);

        String detailPermission = PermissionEnum.getPermissionByCode(permission);

        validateUnknownPermission(detailPermission);

        if (!existPermissionAlbumUser(album.getIdEndpoint(), user.getIdEndpoint())
                && !album.isAuthor(user.getIdEndpoint())) {

            user = userService.userVerified(user);
            album = albumService.albumVerified(album);

            PermissionAlbumUser permissionAlbumUser = PermissionAlbumUser.builder()
                    .user(user)
                    .album(album)
                    .permissions(detailPermission)
                    .build();

            return Optional.of(save(permissionAlbumUser));
        }
        throw new PermissionException("Unknown Error");
    }

    @Override
    public Optional<PermissionAlbumUserDto> updatePermission(Long idAlbum,
                                                             Long idUser,
                                                             Integer permission) {

        String detailPermission = PermissionEnum.getPermissionByCode(permission);

        validateUnknownPermission(detailPermission);

        PermissionAlbumUser permissionAlbumUser = getPermissionByAlbumAndUser(idAlbum, idUser);

        PermissionAlbumUser permissionAlbumUserUpdate = PermissionAlbumUser.builder()
                .id(permissionAlbumUser.getId())
                .user(permissionAlbumUser.getUser())
                .album(permissionAlbumUser.getAlbum())
                .permissions(detailPermission)
                .build();

        return Optional.of(save(permissionAlbumUserUpdate));

    }

    private PermissionAlbumUser getPermissionByAlbumAndUser(Long idAlbum, Long idUser) {

        return permissionAlbumUserRepository.findByAlbum_IdEndpointAndUser_IdEndpoint(idAlbum, idUser)
                .orElseThrow(() -> new PermissionException("Not found"));
    }

    private PermissionAlbumUserDto save(PermissionAlbumUser permissionAlbumUser) {

        return transform.PermissionAlbumUserToPermissionAlbumUserDto(
                permissionAlbumUserRepository.save(permissionAlbumUser));
    }

    private void validateUnknownPermission(String permissionEnum) {

        if (permissionEnum.equals(PermissionEnum.Unknown.getDetail())) {
            throw new PermissionException(PermissionEnum.Unknown.getDetail());
        }
    }

    private Boolean existPermissionAlbumUser(Long idAlbum, Long idUser) {

        return permissionAlbumUserRepository.existsByAlbum_IdEndpointAndUser_IdEndpoint(idAlbum, idUser);
    }
}
