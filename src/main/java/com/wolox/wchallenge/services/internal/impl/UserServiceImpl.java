package com.wolox.wchallenge.services.internal.impl;

import com.wolox.wchallenge.exceptions.UserException;
import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;
import com.wolox.wchallenge.models.enums.PermissionEnum;
import com.wolox.wchallenge.models.transform.UserTransform;
import com.wolox.wchallenge.repositories.UserRepository;
import com.wolox.wchallenge.services.internal.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserTransform userTransform;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserTransform userTransform) {
        this.userRepository = userRepository;
        this.userTransform = userTransform;
    }

    @Override
    public User userVerified(User user) {
        return userRepository.findByIdEndpoint(user.getIdEndpoint())
                .orElseGet(() -> save(user));
    }

    @Override
    public List<UserDto> getUsersByAlbumAndPermission(Long idAlbum, Integer permission) {

        String detailPermission = PermissionEnum.getPermissionByCode(permission);

        if (detailPermission.equals(PermissionEnum.Unknown.getDetail())) {
            throw new UserException(PermissionEnum.Unknown.getDetail());
        }

        return userRepository.getUsersByAlbumWithPermission(idAlbum, detailPermission)
                .stream()
                .map(userTransform::UserToUserDto)
                .collect(Collectors.toList());
    }


    private User save(User user) {
        return userRepository.save(user);
    }
}
