package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.models.dto.CommentDto;
import com.wolox.wchallenge.models.dto.PostDto;
import com.wolox.wchallenge.services.external.CommentExternalService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@PropertySource("classpath:url.properties")
@Service
public class CommentExternalServiceImpl implements CommentExternalService {

    private final PostExternalServiceImpl postExternalService;

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${endpoint.comment.url}")
    private String commentsUrl;

    @Value("${endpoint.comment.filter.name.url}")
    private String commentNameUrl;

    @Value("${endpoint.comment.filter.post.url}")
    private String commentPostUrl;

    public CommentExternalServiceImpl(PostExternalServiceImpl postExternalService) {
        this.postExternalService = postExternalService;
    }

    @Override
    public List<CommentDto> getAllComment() {
        ResponseEntity<List<CommentDto>> response;

        response = restTemplate.exchange(
                commentsUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }

    @Override
    public List<CommentDto> getCommentByName(String name) {
        ResponseEntity<List<CommentDto>> response;
        response = restTemplate.exchange(
                commentNameUrl + name,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }

    @Override
    public List<CommentDto> getCommentByIdUser(Long userId) {
        List<CommentDto> comments = new ArrayList<>();
        postExternalService.getPostByUser(userId)
                .stream()
                .map(PostDto::getId)
                .forEach(post -> comments.addAll(getACommentByPost(post)));

        return comments;
    }

    private List<CommentDto> getACommentByPost(Long postId) {
        ResponseEntity<List<CommentDto>> response;

        response = restTemplate.exchange(
                commentPostUrl + postId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }
}
