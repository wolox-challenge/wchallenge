package com.wolox.wchallenge.services.external;

import com.wolox.wchallenge.models.dto.PhotoDto;

import java.util.List;

public interface PhotoExternalService {

    List<PhotoDto> getAllPhotos();

    PhotoDto getPhotoById(Long photoId);

    List<PhotoDto> getPhotosByAlbum(Long albumId);

    List<PhotoDto> getPhotosByUser(Long userId);
}
