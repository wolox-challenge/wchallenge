package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.models.dto.PostDto;
import com.wolox.wchallenge.services.external.PostExternalService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@PropertySource("classpath:url.properties")
@Service
public class PostExternalServiceImpl implements PostExternalService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${endpoint.post.filter.user.url}")
    private String postUserUrl;

    @Override
    public List<PostDto> getPostByUser(Long userId) {
        ResponseEntity<List<PostDto>> response;
            response = restTemplate.exchange(
                    postUserUrl + userId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();
    }
}
