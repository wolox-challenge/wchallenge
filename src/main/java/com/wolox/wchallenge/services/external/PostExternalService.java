package com.wolox.wchallenge.services.external;

import com.wolox.wchallenge.models.dto.PostDto;

import java.util.List;

public interface PostExternalService {

    List<PostDto> getPostByUser(Long userId);

}
