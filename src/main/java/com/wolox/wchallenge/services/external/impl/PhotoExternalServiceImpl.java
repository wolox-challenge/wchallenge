package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.PhotoException;
import com.wolox.wchallenge.models.dto.AlbumDto;
import com.wolox.wchallenge.models.dto.PhotoDto;
import com.wolox.wchallenge.services.external.AlbumExternalService;
import com.wolox.wchallenge.services.external.PhotoExternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@PropertySource("classpath:url.properties")
@Service
public class PhotoExternalServiceImpl implements PhotoExternalService {

    private final AlbumExternalService albumExternalService;

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${endpoint.photos.url}")
    private String photosUrl;

    @Value("${endpoint.photos.filter.album.url}")
    private String photosAlbumUrl;

    @Autowired
    public PhotoExternalServiceImpl(AlbumExternalService albumExternalService) {
        this.albumExternalService = albumExternalService;
    }

    @Override
    public List<PhotoDto> getAllPhotos() {
        ResponseEntity<List<PhotoDto>> response;

        response = restTemplate.exchange(
                photosUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();

    }

    @Override
    public PhotoDto getPhotoById(Long photoId) {
        ResponseEntity<PhotoDto> response;

        try {
            response = restTemplate.exchange(
                    photosUrl + "/" + photoId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            throw new PhotoException(e.getMessage());
        }
    }

    @Override
    public List<PhotoDto> getPhotosByAlbum(Long albumId) {
        ResponseEntity<List<PhotoDto>> response;

        response = restTemplate.exchange(
                photosAlbumUrl + albumId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();

    }

    @Override
    public List<PhotoDto> getPhotosByUser(Long userId) {
        List<PhotoDto> photos = new ArrayList<>();

        albumExternalService.getAlbumByIdUser(userId)
                .stream()
                .map(AlbumDto::getId)
                .forEach(album -> photos.addAll(getPhotosByAlbum(album)));

        return photos;
    }


}
