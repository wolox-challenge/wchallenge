package com.wolox.wchallenge.services.external;

import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;

import java.util.List;

public interface UserExternalService {

    List<UserDto> getAllUsers();

    UserDto getUserById(Long userId);

    User getUser(Long userId);
}
