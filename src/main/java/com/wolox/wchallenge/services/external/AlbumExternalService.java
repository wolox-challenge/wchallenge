package com.wolox.wchallenge.services.external;

import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.models.dto.AlbumDto;

import java.util.List;

public interface AlbumExternalService {

    List<AlbumDto> getAllAlbum();

    AlbumDto getAlbumById(Long albumId);

    Album getAlbum(Long albumId);

    List<AlbumDto> getAlbumByIdUser(Long userId);
}
