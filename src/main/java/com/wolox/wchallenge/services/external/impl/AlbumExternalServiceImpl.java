package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.AlbumException;
import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.models.dto.AlbumDto;
import com.wolox.wchallenge.services.external.AlbumExternalService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@PropertySource("classpath:url.properties")
@Service
public class AlbumExternalServiceImpl implements AlbumExternalService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${endpoint.albums.url}")
    private String albumsUrl;

    @Value("${endpoint.albums.filter.user.url}")
    private String albumUserUrl;

    @Override
    public List<AlbumDto> getAllAlbum() {
        ResponseEntity<List<AlbumDto>> response;

        response = restTemplate.exchange(
                albumsUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }

    @Override
    public AlbumDto getAlbumById(Long albumId) {
        ResponseEntity<AlbumDto> response;

        try {
            response = restTemplate.exchange(
                    albumsUrl + "/" + albumId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            throw new AlbumException(e.getMessage());
        }
    }

    @Override
    public Album getAlbum(Long albumId) {
        ResponseEntity<Album> response;

        try {
            response = restTemplate.exchange(
                    albumsUrl + "/" + albumId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            throw new AlbumException(e.getMessage());
        }
    }

    @Override
    public List<AlbumDto> getAlbumByIdUser(Long userId) {
        ResponseEntity<List<AlbumDto>> response;

        response = restTemplate.exchange(
                albumUserUrl + userId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();
    }
}
