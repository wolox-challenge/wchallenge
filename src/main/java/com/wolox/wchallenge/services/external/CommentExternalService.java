package com.wolox.wchallenge.services.external;

import com.wolox.wchallenge.models.dto.CommentDto;

import java.util.List;

public interface CommentExternalService {

    List<CommentDto> getAllComment();

    List<CommentDto> getCommentByName(String name);

    List<CommentDto> getCommentByIdUser(Long userId);
}
