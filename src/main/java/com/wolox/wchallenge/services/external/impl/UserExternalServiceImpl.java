package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.UserException;
import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;
import com.wolox.wchallenge.services.external.UserExternalService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@PropertySource("classpath:url.properties")
@Service
public class UserExternalServiceImpl implements UserExternalService {

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${endpoint.users.url}")
    private String usersUrl;

    @Override
    public List<UserDto> getAllUsers() {
        ResponseEntity<List<UserDto>> response;

        response = restTemplate.exchange(
                usersUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        return response.getBody();

    }

    @Override
    public UserDto getUserById(Long userId) {
        ResponseEntity<UserDto> response;

        try {
            response = restTemplate.exchange(
                    usersUrl + "/" + userId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            throw new UserException(e.getMessage());
        }
    }

    @Override
    public User getUser(Long userId) {
        ResponseEntity<User> response;

        try {
            response = restTemplate.exchange(
                    usersUrl + "/" + userId,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    });

            return response.getBody();

        } catch (Exception e) {
            throw new UserException(e.getMessage());
        }
    }

}
