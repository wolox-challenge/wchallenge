package com.wolox.wchallenge.models.dto;

import lombok.Data;

@Data
public class PermissionDto {
    private Long idUser;
    private Long idAlbum;
    private Integer permission;
}
