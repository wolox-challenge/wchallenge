package com.wolox.wchallenge.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    private String street;

    private String suite;

    private String city;

    private String zipcode;

    private GeoDto geo;
}
