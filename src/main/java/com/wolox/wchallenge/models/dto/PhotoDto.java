package com.wolox.wchallenge.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhotoDto {

    private Long id;

    private Long albumId;

    private String title;

    private String url;

    private String thumbnailUrl;

}
