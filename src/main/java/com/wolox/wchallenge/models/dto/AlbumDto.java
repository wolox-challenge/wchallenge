package com.wolox.wchallenge.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDto {

    private Long id;

    private Long userId;

    private String title;
}
