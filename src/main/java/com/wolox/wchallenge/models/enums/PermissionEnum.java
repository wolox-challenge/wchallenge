package com.wolox.wchallenge.models.enums;

import java.util.Arrays;

public enum PermissionEnum {

    Unknown(0, "Unknown"),
    Read(1, "Read"),
    Write(2, "Write"),
    Total(3, "Read_Write");

    private final Integer code;
    private final String detail;

    PermissionEnum(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getPermissionByCode(Integer code) {

        PermissionEnum permission = Arrays.stream(values()).filter(value -> value.code.equals(code))
                .findFirst()
                .orElse(null);

        return permission.detail;
    }

    public Integer getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }
}
