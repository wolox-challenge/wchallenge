package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.Address;
import com.wolox.wchallenge.models.dto.AddressDto;
import org.springframework.stereotype.Component;

@Component
public class AddressTransform {

    public static final GeoTransform geoTransform = new GeoTransform();

    public AddressDto addressToAddressDto(Address address) {

        AddressDto addressResponse = null;

        if (null != address) {
            addressResponse = AddressDto.builder()
                    .city(address.getCity())
                    .street(address.getStreet())
                    .suite(address.getSuite())
                    .zipcode(address.getZipcode())
                    .geo(geoTransform.geoToGeoDto(address.getGeo()))
                    .build();
        }
        return addressResponse;
    }
}
