package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.Company;
import com.wolox.wchallenge.models.dto.CompanyDto;
import org.springframework.stereotype.Component;

@Component
public class CompanyTransform {

    public CompanyDto CompanyToCompanyDto(Company company) {
        CompanyDto companyResponse = null;

        if (null != company) {
            companyResponse = CompanyDto.builder()
                    .name(company.getName())
                    .bs(company.getBs())
                    .catchPhrase(company.getCatchPhrase())
                    .build();
        }

        return companyResponse;
    }
}
