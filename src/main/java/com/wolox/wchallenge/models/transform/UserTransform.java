package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserTransform {

    public static final CompanyTransform companyTransform = new CompanyTransform();
    public static final AddressTransform addressTransform = new AddressTransform();

    public UserDto UserToUserDto(User user) {
        UserDto userResponse = null;
        if (null != user) {
            userResponse = UserDto.builder()
                    .id(user.getIdEndpoint())
                    .email(user.getEmail())
                    .name(user.getName())
                    .phone(user.getPhone())
                    .username(user.getUsername())
                    .website(user.getWebsite())
                    .address(addressTransform.addressToAddressDto(user.getAddress()))
                    .company(companyTransform.CompanyToCompanyDto(user.getCompany()))
                    .build();
        }

        return userResponse;
    }
}
