package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.PermissionAlbumUser;
import com.wolox.wchallenge.models.dto.PermissionAlbumUserDto;
import org.springframework.stereotype.Component;

@Component
public class PermissionAlbumUserTransform {

    private static final UserTransform userTransform = new UserTransform();
    private static final AlbumTransform albumTransform = new AlbumTransform();

    public PermissionAlbumUserDto PermissionAlbumUserToPermissionAlbumUserDto(PermissionAlbumUser permissionAlbumUser) {
        PermissionAlbumUserDto permissionAlbumUserResponse = null;

        if (null != permissionAlbumUser) {
            permissionAlbumUserResponse = PermissionAlbumUserDto.builder()
                    .id(permissionAlbumUser.getId())
                    .permissions(permissionAlbumUser.getPermissions())
                    .user(userTransform.UserToUserDto(permissionAlbumUser.getUser()))
                    .album(albumTransform.AlbumToAlbumDto(permissionAlbumUser.getAlbum()))
                    .build();
        }

        return permissionAlbumUserResponse;
    }
}
