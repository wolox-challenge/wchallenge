package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.Geo;
import com.wolox.wchallenge.models.dto.GeoDto;
import org.springframework.stereotype.Component;

@Component
public class GeoTransform {

    public GeoDto geoToGeoDto(Geo geo) {
        GeoDto geoResponse = null;

        if (null != geo) {
            geoResponse = GeoDto.builder()
                    .lat(geo.getLat())
                    .lng(geo.getLng())
                    .build();
        }

        return geoResponse;
    }
}
