package com.wolox.wchallenge.models.transform;

import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.models.dto.AlbumDto;
import org.springframework.stereotype.Component;

@Component
public class AlbumTransform {

    public AlbumDto AlbumToAlbumDto(Album album) {
        AlbumDto albumResponse = null;

        if (null != album) {
            albumResponse = AlbumDto.builder()
                    .id(album.getIdEndpoint())
                    .userId(album.getIdEndpointUser())
                    .title(album.getTitle())
                    .build();
        }

        return albumResponse;
    }
}
