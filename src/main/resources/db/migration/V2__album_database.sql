CREATE TABLE album
(
    id               serial       NOT NULL,
    id_endpoint_user int          NOT NULL,
    id_endpoint      int          NOT NULL,
    title            varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE permission_album_user
(
    id          serial      NOT NULL,
    id_album    int         NOT NULL,
    id_user     int         NOT NULL,
    permissions varchar(10) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE permission_album_user
    ADD CONSTRAINT fk_album_permission FOREIGN KEY (id_album) REFERENCES album (id);
ALTER TABLE permission_album_user
    ADD CONSTRAINT fk_user_permission FOREIGN KEY (id_user) REFERENCES users (id);
