CREATE TABLE address
(
    id      serial       NOT NULL,
    street  varchar(255) NOT NULL,
    suite   varchar(255) NOT NULL,
    city    varchar(255) NOT NULL,
    zipcode varchar(255) NOT NULL,
    geo_id  int          NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE company
(
    id           serial       NOT NULL,
    name         varchar(255) NOT NULL,
    catch_phrase varchar(255) NOT NULL,
    bs           varchar(255) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE geo
(
    id  serial       NOT NULL,
    lat varchar(255) NOT NULL,
    lng varchar(255) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE users
(
    id          serial       NOT NULL,
    id_endpoint int          NOT NULL,
    username    varchar(255) NOT NULL,
    name        varchar(255) NOT NULL,
    email       varchar(255) NOT NULL,
    phone       varchar(255) NOT NULL,
    website     varchar(255) NOT NULL,
    company_id  int          NOT NULL,
    address_id  int          NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE address
    ADD CONSTRAINT fk_address_geo FOREIGN KEY (geo_id) REFERENCES geo (id);
ALTER TABLE users
    ADD CONSTRAINT fk_user_company FOREIGN KEY (company_id) REFERENCES company (id);
ALTER TABLE users
    ADD CONSTRAINT fk_user_address FOREIGN KEY (address_id) REFERENCES address (id);
