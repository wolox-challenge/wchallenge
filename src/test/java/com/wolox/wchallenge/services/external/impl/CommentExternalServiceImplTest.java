package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.models.dto.CommentDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
class CommentExternalServiceImplTest {

    @Autowired
    CommentExternalServiceImpl commentExternalService;
    DataFactory dataFactory;
    String EXIST_NAME = "id labore ex et quam laborum";

    String DONT_EXIST_NAME = "id labores ex et quam laborum";

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
    }

    @Test
    void getAllComment() {
        List<CommentDto> response = commentExternalService.getAllComment();
        assertThat(response).isNotEmpty();
    }

    @Test
    void getCommentExistName() {
        List<CommentDto> response = commentExternalService.getCommentByName(EXIST_NAME);
        assertThat(response).isNotEmpty();
    }

    @Test
    void getCommentNotExistName() {
        List<CommentDto> response = commentExternalService.getCommentByName(DONT_EXIST_NAME);
        assertThat(response).isEmpty();
    }

    @Test
    void getCommentExistUser() {
        List<CommentDto> response = commentExternalService.getCommentByIdUser((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotEmpty();
    }

    @Test
    void getCommentNotExistUser() {
        List<CommentDto> response = commentExternalService.getCommentByIdUser(-1l);
        assertThat(response).isEmpty();
    }
}