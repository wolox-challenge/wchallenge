package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.models.dto.PostDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
class PostExternalServiceImplTest {

    @Autowired
    PostExternalServiceImpl postExternalService;
    DataFactory dataFactory;

    @BeforeEach
    public void setup() {
        dataFactory = new DataFactory();
    }


    @Test
    void getPostExistUser() {
        List<PostDto> response = postExternalService.getPostByUser((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }
}