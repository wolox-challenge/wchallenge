package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.UserException;
import com.wolox.wchallenge.models.User;
import com.wolox.wchallenge.models.dto.UserDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest()
class UserExternalServiceImplTest {

    @Autowired
    UserExternalServiceImpl userExternalService;

    DataFactory dataFactory;

    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
    }

    @Test
    void getAllUsers() {
        List<UserDto> response = userExternalService.getAllUsers();
        assertThat(response).isNotEmpty();
    }

    @Test
    void getUserDtoExist() {
        UserDto response = userExternalService.getUserById((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getUserDtoNoExist() {
        Exception exception = Assertions.assertThrows(UserException.class, () -> userExternalService.getUserById(-1L));
        assertThat(exception).isNotNull();
    }

    @Test
    void getUserExist() {
        User response = userExternalService.getUser((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getUserNoExist() {
        Exception exception = Assertions.assertThrows(UserException.class, () -> userExternalService.getUser(-1L));
        assertThat(exception).isNotNull();
    }
}