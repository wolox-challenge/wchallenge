package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.AlbumException;
import com.wolox.wchallenge.models.Album;
import com.wolox.wchallenge.models.dto.AlbumDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
class AlbumExternalServiceImplTest {

    @Autowired
    AlbumExternalServiceImpl albumExternalService;
    DataFactory dataFactory;


    @BeforeEach
    void setUp() {
        dataFactory = new DataFactory();
    }

    @Test
    void getAllAlbum() {
        List<AlbumDto> response = albumExternalService.getAllAlbum();
        assertThat(response).isNotEmpty();
    }

    @Test
    void getAlbumDtoExistId() {
        AlbumDto response = albumExternalService.getAlbumById((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getAlbumDtoNotExistId() {
        Exception exception = Assertions.assertThrows(AlbumException.class, () -> albumExternalService.getAlbumById(-1L));

        assertThat(exception).isNotNull();
    }

    @Test
    void getAlbumByIdUser() {
        List<AlbumDto> response = albumExternalService.getAlbumByIdUser((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotEmpty();
    }

    @Test
    void getAlbumExistId() {
        Album response = albumExternalService.getAlbum((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getAlbumNotExistId() {
        Exception exception = Assertions.assertThrows(AlbumException.class, () -> albumExternalService.getAlbum(-1L));

        assertThat(exception).isNotNull();
    }
}