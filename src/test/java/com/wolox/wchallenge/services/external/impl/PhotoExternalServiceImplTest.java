package com.wolox.wchallenge.services.external.impl;

import com.wolox.wchallenge.exceptions.PhotoException;
import com.wolox.wchallenge.models.dto.PhotoDto;
import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
class PhotoExternalServiceImplTest {

    @Autowired
    PhotoExternalServiceImpl photoExternalServiceImpl;
    DataFactory dataFactory;

    @BeforeEach
    public void setup() {
        dataFactory = new DataFactory();
    }

    @Test
    void getAllPhotos() {
        List<PhotoDto> response = photoExternalServiceImpl.getAllPhotos();
        assertThat(response).isNotEmpty();
    }

    @Test
    void getPhotoExistId() {
        PhotoDto response = photoExternalServiceImpl.getPhotoById((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getPhotoNotExistId() {
        Exception exception = Assertions.assertThrows(PhotoException.class, () -> photoExternalServiceImpl.getPhotoById(-1L));
        assertThat(exception).isNotNull();
    }

    @Test
    void getPhotosExistAlbum() {
        List<PhotoDto> response = photoExternalServiceImpl.getPhotosByAlbum((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }

    @Test
    void getPhotosExistUser() {
        List<PhotoDto> response = photoExternalServiceImpl.getPhotosByUser((long) dataFactory.getNumberBetween(1, 10));
        assertThat(response).isNotNull();
    }
}